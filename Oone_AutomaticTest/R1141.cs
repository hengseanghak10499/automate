﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Oone_AutomaticTest
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R1141 recording.
    /// </summary>
    [TestModule("4fe87732-c2b3-4f9c-ad6b-30089bc89451", ModuleType.Recording, 1)]
    public partial class R1141 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the Oone_AutomaticTestRepository repository.
        /// </summary>
        public static Oone_AutomaticTestRepository repo = Oone_AutomaticTestRepository.Instance;

        static R1141 instance = new R1141();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R1141()
        {
            CustomerID = "";
            Detail_Mics = "";
            Payment = "";
            Shortcut = "";
            note = "";
            GetTotal = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R1141 Instance
        {
            get { return instance; }
        }

#region Variables

        string _CustomerID;

        /// <summary>
        /// Gets or sets the value of variable CustomerID.
        /// </summary>
        [TestVariable("28359a43-279b-4b76-9bad-bf65a970e121")]
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }

        string _Detail_Mics;

        /// <summary>
        /// Gets or sets the value of variable Detail_Mics.
        /// </summary>
        [TestVariable("a5aa29b0-08ab-4224-92ab-07130e483cd1")]
        public string Detail_Mics
        {
            get { return _Detail_Mics; }
            set { _Detail_Mics = value; }
        }

        string _Payment;

        /// <summary>
        /// Gets or sets the value of variable Payment.
        /// </summary>
        [TestVariable("c37086ea-b9d7-4509-a0a1-6e9c204b7cac")]
        public string Payment
        {
            get { return _Payment; }
            set { _Payment = value; }
        }

        string _Shortcut;

        /// <summary>
        /// Gets or sets the value of variable Shortcut.
        /// </summary>
        [TestVariable("b35bcc42-9c62-4fef-8732-20bfb21cf3bc")]
        public string Shortcut
        {
            get { return _Shortcut; }
            set { _Shortcut = value; }
        }

        string _note;

        /// <summary>
        /// Gets or sets the value of variable note.
        /// </summary>
        [TestVariable("8d2ff7c7-bfc6-4ef5-ba56-7b5a65ce4b8b")]
        public string note
        {
            get { return _note; }
            set { _note = value; }
        }

        string _GetTotal;

        /// <summary>
        /// Gets or sets the value of variable GetTotal.
        /// </summary>
        [TestVariable("d26cdc31-a907-49ce-82fe-082c6ae35870")]
        public string GetTotal
        {
            get { return _GetTotal; }
            set { _GetTotal = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.StormPaymentInterfacesMiscIncomePage' at 74;15.", repo.MainForm.MainPanel.StormPaymentInterfacesMiscIncomePageInfo, new RecordItemIndex(0));
            repo.MainForm.MainPanel.StormPaymentInterfacesMiscIncomePage.Click("74;15");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnAdd' at 19;13.", repo.MainForm.MainPanel.BtnAddInfo, new RecordItemIndex(1));
            repo.MainForm.MainPanel.BtnAdd.Click("19;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.TxtSearch' at 93;11.", repo.MiscIncomeDialog.TxtSearchInfo, new RecordItemIndex(2));
            repo.MiscIncomeDialog.TxtSearch.Click("93;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$CustomerID' with focus on 'MiscIncomeDialog.TxtSearch'.", repo.MiscIncomeDialog.TxtSearchInfo, new RecordItemIndex(3));
            repo.MiscIncomeDialog.TxtSearch.PressKeys(CustomerID);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.Button' at 13;14.", repo.MiscIncomeDialog.ButtonInfo, new RecordItemIndex(4));
            repo.MiscIncomeDialog.Button.Click("13;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 2s.", new RecordItemIndex(5));
            Delay.Duration(2000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.បនថម' at 9;7.", repo.MiscIncomeDialog.បនថមInfo, new RecordItemIndex(6));
            repo.MiscIncomeDialog.បនថម.Click("9;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 2s.", new RecordItemIndex(7));
            Delay.Duration(2000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.Open' at 7;9.", repo.MiscIncomeDialog.OpenInfo, new RecordItemIndex(8));
            repo.MiscIncomeDialog.Open.Click("7;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.TxtSearch' at 82;13.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(9));
            repo.SelectItemsDialog.TxtSearch.Click("82;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$Detail_Mics' with focus on 'SelectItemsDialog.TxtSearch'.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(10));
            repo.SelectItemsDialog.TxtSearch.PressKeys(Detail_Mics);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.Button' at 10;11.", repo.SelectItemsDialog.ButtonInfo, new RecordItemIndex(11));
            repo.SelectItemsDialog.Button.Click("10;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(12));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.BtnSelect' at 53;8.", repo.SelectItemsDialog.BtnSelectInfo, new RecordItemIndex(13));
            repo.SelectItemsDialog.BtnSelect.Click("53;8");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.TableDgv1.ទកបរកRow0' at 110;6.", repo.MiscIncomeDialog.TableDgv1.ទកបរកRow0Info, new RecordItemIndex(14));
            repo.MiscIncomeDialog.TableDgv1.ទកបរកRow0.Click("110;6");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$Payment' with focus on 'MiscIncomeDialog.TableDgv1.ទកបរកRow0'.", repo.MiscIncomeDialog.TableDgv1.ទកបរកRow0Info, new RecordItemIndex(15));
            repo.MiscIncomeDialog.TableDgv1.ទកបរកRow0.PressKeys(Payment);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Down item 'MiscIncomeDialog.Text' at 274;26.", repo.MiscIncomeDialog.TextInfo, new RecordItemIndex(16));
            repo.MiscIncomeDialog.Text.MoveTo("274;26");
            Mouse.ButtonDown(System.Windows.Forms.MouseButtons.Left);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Up item 'MiscIncomeDialog.Text' at 274;26.", repo.MiscIncomeDialog.TextInfo, new RecordItemIndex(17));
            repo.MiscIncomeDialog.Text.MoveTo("274;26");
            Mouse.ButtonUp(System.Windows.Forms.MouseButtons.Left);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{LControlKey up}' with focus on 'MiscIncomeDialog'.", repo.MiscIncomeDialog.SelfInfo, new RecordItemIndex(18));
            repo.MiscIncomeDialog.Self.EnsureVisible();
            Keyboard.Press("{LControlKey up}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{LControlKey up}' with focus on 'MiscIncomeDialog.Text'.", repo.MiscIncomeDialog.TextInfo, new RecordItemIndex(19));
            repo.MiscIncomeDialog.Text.PressKeys("{LControlKey up}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key from variable $Shortcut Press with focus on 'MiscIncomeDialog'.", repo.MiscIncomeDialog.SelfInfo, new RecordItemIndex(20));
            Keyboard.PrepareFocus(repo.MiscIncomeDialog.Self);
            Keyboard.Press(Keyboard.ToKey(Shortcut), 3, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$note' with focus on 'MiscIncomeDialog.Text'.", repo.MiscIncomeDialog.TextInfo, new RecordItemIndex(21));
            repo.MiscIncomeDialog.Text.PressKeys(note);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.ChkPrintInvoice' at 5;10.", repo.MiscIncomeDialog.ChkPrintInvoiceInfo, new RecordItemIndex(22));
            repo.MiscIncomeDialog.ChkPrintInvoice.Click("5;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'Text' from item 'MiscIncomeDialog.TxtTotal' and assigning its value to variable 'GetTotal'.", repo.MiscIncomeDialog.TxtTotalInfo, new RecordItemIndex(23));
            GetTotal = repo.MiscIncomeDialog.TxtTotal.Element.GetAttributeValueText("Text");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.BtnSave' at 56;9.", repo.MiscIncomeDialog.BtnSaveInfo, new RecordItemIndex(24));
            repo.MiscIncomeDialog.BtnSave.Click("56;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(25));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MiscIncomeDialog.BtnClose' at 45;4.", repo.MiscIncomeDialog.BtnCloseInfo, new RecordItemIndex(26));
            repo.MiscIncomeDialog.BtnClose.Click("45;4");
            Delay.Milliseconds(11420);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
