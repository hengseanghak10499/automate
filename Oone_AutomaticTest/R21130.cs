﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Oone_AutomaticTest
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R21130 recording.
    /// </summary>
    [TestModule("61ba8e52-9e65-40c4-9fc5-69a9932fdd78", ModuleType.Recording, 1)]
    public partial class R21130 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the Oone_AutomaticTestRepository repository.
        /// </summary>
        public static Oone_AutomaticTestRepository repo = Oone_AutomaticTestRepository.Instance;

        static R21130 instance = new R21130();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R21130()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R21130 Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.Open8' at 2;10.", repo.Qt5QWindowToolSaveBits.Open8Info, new RecordItemIndex(0));
            repo.Qt5QWindowToolSaveBits.Open8.Click("2;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.ឈមRow0' at 80;15.", repo.Qt5QWindowToolSaveBits.ឈមRow0Info, new RecordItemIndex(1));
            repo.Qt5QWindowToolSaveBits.ឈមRow0.Click("80;15");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.Container.BtnSelect' at 54;10.", repo.Qt5QWindowToolSaveBits.Container.BtnSelectInfo, new RecordItemIndex(2));
            repo.Qt5QWindowToolSaveBits.Container.BtnSelect.Click("54;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Tab}' with focus on 'Qt5QWindowToolSaveBits'.", repo.Qt5QWindowToolSaveBits.SelfInfo, new RecordItemIndex(3));
            repo.Qt5QWindowToolSaveBits.Self.EnsureVisible();
            Keyboard.Press("{Tab}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Explorer.IMEModeButton' at 0;0.", repo.Explorer.IMEModeButtonInfo, new RecordItemIndex(4));
            repo.Explorer.IMEModeButton.Click("0;0");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
