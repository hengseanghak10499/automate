﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Oone_AutomaticTest
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R2119 recording.
    /// </summary>
    [TestModule("41c4c252-e172-4782-ab13-bb977b4b153d", ModuleType.Recording, 1)]
    public partial class R2119 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the Oone_AutomaticTestRepository repository.
        /// </summary>
        public static Oone_AutomaticTestRepository repo = Oone_AutomaticTestRepository.Instance;

        static R2119 instance = new R2119();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R2119()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R2119 Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.ImgPattern' at 16;14.", repo.MainForm.MainPanel.ImgPatternInfo, new RecordItemIndex(0));
            repo.MainForm.MainPanel.ImgPattern.Click("16;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MenuPattern.លខគណនបរបរសIDចស' at 55;4.", repo.MenuPattern.លខគណនបរបរសIDចសInfo, new RecordItemIndex(1));
            repo.MenuPattern.លខគណនបរបរសIDចស.Click("55;4");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.TxtSearch10' at 18;12.", repo.MainForm.MainPanel.TxtSearch10Info, new RecordItemIndex(2));
            repo.MainForm.MainPanel.TxtSearch10.Click("18;12");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '4189810' with focus on 'MainForm.MainPanel.TxtSearch10'.", repo.MainForm.MainPanel.TxtSearch10Info, new RecordItemIndex(3));
            repo.MainForm.MainPanel.TxtSearch10.PressKeys("4189810");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Button' at 12;9.", repo.MainForm.ButtonInfo, new RecordItemIndex(4));
            repo.MainForm.Button.Click("12;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnUpdate7' at 41;8.", repo.MainForm.MainPanel.BtnUpdate7Info, new RecordItemIndex(5));
            repo.MainForm.MainPanel.BtnUpdate7.Click("41;8");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Text='5304189810 កាន់ ឡាយ') on item 'Qt5QWindowToolSaveBits.TabCustomerInformation.CellRow11'.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.CellRow11Info, new RecordItemIndex(6));
            Validate.AttributeEqual(repo.Qt5QWindowToolSaveBits.TabCustomerInformation.CellRow11Info, "Text", "5304189810 កាន់ ឡាយ");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(7));
            Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.Container.BtnClose' at 49;10.", repo.Qt5QWindowToolSaveBits.Container.BtnCloseInfo, new RecordItemIndex(8));
            repo.Qt5QWindowToolSaveBits.Container.BtnClose.Click("49;10");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
