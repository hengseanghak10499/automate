﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Oone_AutomaticTest
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R2112 recording.
    /// </summary>
    [TestModule("e295f675-988b-4b0c-873f-d21b93c36b72", ModuleType.Recording, 1)]
    public partial class R2112 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the Oone_AutomaticTestRepository repository.
        /// </summary>
        public static Oone_AutomaticTestRepository repo = Oone_AutomaticTestRepository.Instance;

        static R2112 instance = new R2112();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R2112()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R2112 Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.TxtSearch10' at 52;10.", repo.MainForm.MainPanel.TxtSearch10Info, new RecordItemIndex(0));
            repo.MainForm.MainPanel.TxtSearch10.Click("52;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{F4}' with focus on 'MainForm.MainPanel.TxtSearch10'.", repo.MainForm.MainPanel.TxtSearch10Info, new RecordItemIndex(1));
            repo.MainForm.MainPanel.TxtSearch10.PressKeys("{F4}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '5302012612' with focus on 'MainForm.MainPanel.TxtSearch10'.", repo.MainForm.MainPanel.TxtSearch10Info, new RecordItemIndex(2));
            repo.MainForm.MainPanel.TxtSearch10.PressKeys("5302012612");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Button' at 8;5.", repo.MainForm.ButtonInfo, new RecordItemIndex(3));
            repo.MainForm.Button.Click("8;5");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 3s.", new RecordItemIndex(4));
            Delay.Duration(3000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnUpdate7' at 55;12.", repo.MainForm.MainPanel.BtnUpdate7Info, new RecordItemIndex(5));
            repo.MainForm.MainPanel.BtnUpdate7.Click("55;12");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(6));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.Container.BtnClose' at 46;5.", repo.Qt5QWindowToolSaveBits.Container.BtnCloseInfo, new RecordItemIndex(7));
            repo.Qt5QWindowToolSaveBits.Container.BtnClose.Click("46;5");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Button' at 7;11.", repo.MainForm.ButtonInfo, new RecordItemIndex(8));
            repo.MainForm.Button.Click("7;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Button' at 11;6.", repo.MainForm.ButtonInfo, new RecordItemIndex(9));
            repo.MainForm.Button.Click("11;6");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
