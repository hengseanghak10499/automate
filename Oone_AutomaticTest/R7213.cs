﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Oone_AutomaticTest
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R7213 recording.
    /// </summary>
    [TestModule("bc0a3473-f16d-4519-b384-ad252fe0b145", ModuleType.Recording, 1)]
    public partial class R7213 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the Oone_AutomaticTestRepository repository.
        /// </summary>
        public static Oone_AutomaticTestRepository repo = Oone_AutomaticTestRepository.Instance;

        static R7213 instance = new R7213();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R7213()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R7213 Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(0));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Ctrl+O' Press with focus on 'MainForm'.", repo.MainForm.SelfInfo, new RecordItemIndex(1));
            Keyboard.PrepareFocus(repo.MainForm.Self);
            Keyboard.Press(System.Windows.Forms.Keys.O | System.Windows.Forms.Keys.Control, 24, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(2));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Ctrl+Q' Press with focus on 'AuditTrailDetailDialog'.", repo.AuditTrailDetailDialog.SelfInfo, new RecordItemIndex(3));
            Keyboard.PrepareFocus(repo.AuditTrailDetailDialog.Self);
            Keyboard.Press(System.Windows.Forms.Keys.Q | System.Windows.Forms.Keys.Control, Keyboard.DefaultScanCode, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
