﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Oone_AutomaticTest
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R21113 recording.
    /// </summary>
    [TestModule("f80b9820-93c0-4525-84b3-95fe6727c129", ModuleType.Recording, 1)]
    public partial class R21113 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the Oone_AutomaticTestRepository repository.
        /// </summary>
        public static Oone_AutomaticTestRepository repo = Oone_AutomaticTestRepository.Instance;

        static R21113 instance = new R21113();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R21113()
        {
            Name = "សេង​មិនា";
            ID_Card = "01233456";
            Phone_number = "078 779 987";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R21113 Instance
        {
            get { return instance; }
        }

#region Variables

        string _Name;

        /// <summary>
        /// Gets or sets the value of variable Name.
        /// </summary>
        [TestVariable("f2a3342d-dc73-49dc-890a-9d22df325dd8")]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        string _ID_Card;

        /// <summary>
        /// Gets or sets the value of variable ID_Card.
        /// </summary>
        [TestVariable("75688a29-972e-49ed-99a5-b94199224e9e")]
        public string ID_Card
        {
            get { return _ID_Card; }
            set { _ID_Card = value; }
        }

        string _Phone_number;

        /// <summary>
        /// Gets or sets the value of variable Phone_number.
        /// </summary>
        [TestVariable("e3563dd2-7a2b-4b78-b877-74a0ce4a194b")]
        public string Phone_number
        {
            get { return _Phone_number; }
            set { _Phone_number = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable Index.
        /// </summary>
        [TestVariable("94057c41-56a2-4487-88d4-f343cacbe3b8")]
        public string Index
        {
            get { return repo.Index; }
            set { repo.Index = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnAdd9' at 44;16.", repo.MainForm.MainPanel.BtnAdd9Info, new RecordItemIndex(0));
            repo.MainForm.MainPanel.BtnAdd9.Click("44;16");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabCustomerInformation.Open5' at 8;17.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Open5Info, new RecordItemIndex(1));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Open5.Click("8;17");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'SelectItemsDialog.ឈមRow1' at 29;16.", repo.SelectItemsDialog.ឈមRow1Info, new RecordItemIndex(2));
            repo.SelectItemsDialog.ឈមRow1.DoubleClick("29;16");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.Text' at 23;8.", repo.Qt5QWindowToolSaveBits.TextInfo, new RecordItemIndex(3));
            repo.Qt5QWindowToolSaveBits.Text.Click("23;8");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$Name' with focus on 'Qt5QWindowToolSaveBits.Text'.", repo.Qt5QWindowToolSaveBits.TextInfo, new RecordItemIndex(4));
            repo.Qt5QWindowToolSaveBits.Text.PressKeys(Name);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabCustomerInformation.CboSex' at 45;9.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.CboSexInfo, new RecordItemIndex(5));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.CboSex.Click("45;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.ឈមRow1' at 40;13.", repo.SelectItemsDialog.ឈមRow1Info, new RecordItemIndex(6));
            repo.SelectItemsDialog.ឈមRow1.Click("40;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.BtnSelect' at 41;8.", repo.SelectItemsDialog.BtnSelectInfo, new RecordItemIndex(7));
            repo.SelectItemsDialog.BtnSelect.Click("41;8");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabCustomerInformation.Text4' at 30;10.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text4Info, new RecordItemIndex(8));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text4.Click("30;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$ID_Card' with focus on 'Qt5QWindowToolSaveBits.TabCustomerInformation.Text4'.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text4Info, new RecordItemIndex(9));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text4.PressKeys(ID_Card);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabCustomerInformation.Open6' at 4;5.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Open6Info, new RecordItemIndex(10));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Open6.Click("4;5");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'SelectItemsDialog.ឈមRow1' at 33;21.", repo.SelectItemsDialog.ឈមRow1Info, new RecordItemIndex(11));
            repo.SelectItemsDialog.ឈមRow1.DoubleClick("33;21");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabCustomerInformation.Text' at 16;9.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.TextInfo, new RecordItemIndex(12));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text.Click("16;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$Phone_number' with focus on 'Qt5QWindowToolSaveBits.TabCustomerInformation.Text'.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.TextInfo, new RecordItemIndex(13));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text.PressKeys(Phone_number);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabCustomerInformation.Text5' at 27;14.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text5Info, new RecordItemIndex(14));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.Text5.Click("27;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabCustomerInformation.LblAddress' at 9;5.", repo.Qt5QWindowToolSaveBits.TabCustomerInformation.LblAddressInfo, new RecordItemIndex(15));
            repo.Qt5QWindowToolSaveBits.TabCustomerInformation.LblAddress.Click("9;5");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'AddressDialog.Container.CboCommune' at 113;9.", repo.AddressDialog.Container.CboCommuneInfo, new RecordItemIndex(16));
            repo.AddressDialog.Container.CboCommune.Click("113;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'List10001.បឡករ១រល០១០រល0907' at 74;6.", repo.List10001.បឡករ១រល០១០រល0907Info, new RecordItemIndex(17));
            repo.List10001.បឡករ១រល០១០រល0907.Click("74;6");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'AddressDialog.Container.CboVillage' at 37;14.", repo.AddressDialog.Container.CboVillageInfo, new RecordItemIndex(18));
            repo.AddressDialog.Container.CboVillage.Click("37;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'List10001.បឡករ១រល០១០រល0907' at 27;7.", repo.List10001.បឡករ១រល០១០រល0907Info, new RecordItemIndex(19));
            repo.List10001.បឡករ១រល០១០រល0907.Click("27;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'AddressDialog.Container.BtnOK' at 53;17.", repo.AddressDialog.Container.BtnOKInfo, new RecordItemIndex(20));
            repo.AddressDialog.Container.BtnOK.Click("53;17");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.Container.BtnSave' at 47;16.", repo.Qt5QWindowToolSaveBits.Container.BtnSaveInfo, new RecordItemIndex(21));
            repo.Qt5QWindowToolSaveBits.Container.BtnSave.Click("47;16");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 30s.", new RecordItemIndex(22));
            Delay.Duration(30000, false);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating CompareImage (Screenshot: 'Screenshot1' with region {X=0,Y=0,Width=148,Height=24}) on item 'Qt5QWindowToolSaveBits.Container.ទតងបរបរសចរនតIN'.", repo.Qt5QWindowToolSaveBits.Container.ទតងបរបរសចរនតINInfo, new RecordItemIndex(23));
            Validate.CompareImage(repo.Qt5QWindowToolSaveBits.Container.ទតងបរបរសចរនតINInfo, ទតងបរបរសចរនតIN_Screenshot1, ទតងបរបរសចរនតIN_Screenshot1_Options);
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011' at 121;7.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011Info, new RecordItemIndex(24));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011.Click("121;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'RD' with focus on 'Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011'.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011Info, new RecordItemIndex(25));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011.PressKeys("RD");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Return}' with focus on 'Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011'.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011Info, new RecordItemIndex(26));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10011.PressKeys("{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.BtnSelect' at 47;11.", repo.SelectItemsDialog.BtnSelectInfo, new RecordItemIndex(27));
            repo.SelectItemsDialog.BtnSelect.Click("47;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012' at 166;9.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012Info, new RecordItemIndex(28));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012.Click("166;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'R00' with focus on 'Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012'.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012Info, new RecordItemIndex(29));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012.PressKeys("R00");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Return}' with focus on 'Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012'.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012Info, new RecordItemIndex(30));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Text10012.PressKeys("{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabInstallationInformation.Open6' at 4;8.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Open6Info, new RecordItemIndex(31));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Open6.Click("4;8");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.TxtSearch' at 68;11.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(32));
            repo.SelectItemsDialog.TxtSearch.Click("68;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '1Phx20A' with focus on 'SelectItemsDialog.TxtSearch'.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(33));
            repo.SelectItemsDialog.TxtSearch.PressKeys("1Phx20A");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.Button' at 11;4.", repo.SelectItemsDialog.ButtonInfo, new RecordItemIndex(34));
            repo.SelectItemsDialog.Button.Click("11;4");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.BtnSelect' at 47;12.", repo.SelectItemsDialog.BtnSelectInfo, new RecordItemIndex(35));
            repo.SelectItemsDialog.BtnSelect.Click("47;12");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabInstallationInformation.Open' at 10;6.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.OpenInfo, new RecordItemIndex(36));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Open.Click("10;6");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.TxtSearch' at 25;7.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(37));
            repo.SelectItemsDialog.TxtSearch.Click("25;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{NumPad0}{NumPad0}{NumPad3}' with focus on 'SelectItemsDialog.TxtSearch'.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(38));
            repo.SelectItemsDialog.TxtSearch.PressKeys("{NumPad0}{NumPad0}{NumPad3}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.Button' at 14;1.", repo.SelectItemsDialog.ButtonInfo, new RecordItemIndex(39));
            repo.SelectItemsDialog.Button.Click("14;1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.BtnSelect' at 61;13.", repo.SelectItemsDialog.BtnSelectInfo, new RecordItemIndex(40));
            repo.SelectItemsDialog.BtnSelect.Click("61;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.TabInstallationInformation.Open1' at 7;15.", repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Open1Info, new RecordItemIndex(41));
            repo.Qt5QWindowToolSaveBits.TabInstallationInformation.Open1.Click("7;15");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.BtnSelect' at 61;9.", repo.SelectItemsDialog.BtnSelectInfo, new RecordItemIndex(42));
            repo.SelectItemsDialog.BtnSelect.Click("61;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Qt5QWindowToolSaveBits.Container.BtnSave' at 36;12.", repo.Qt5QWindowToolSaveBits.Container.BtnSaveInfo, new RecordItemIndex(43));
            repo.Qt5QWindowToolSaveBits.Container.BtnSave.Click("36;12");
            Delay.Milliseconds(7170);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Ctrl+N' Press with focus on 'Qt5QWindowToolSaveBits'.", repo.Qt5QWindowToolSaveBits.SelfInfo, new RecordItemIndex(44));
            Keyboard.PrepareFocus(repo.Qt5QWindowToolSaveBits.Self);
            Keyboard.Press(System.Windows.Forms.Keys.N | System.Windows.Forms.Keys.Control, 49, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Title='ព័ត៌មានអតិថិជន') on item 'Qt5QWindowToolSaveBits.Container.ពតមនអតថជន'.", repo.Qt5QWindowToolSaveBits.Container.ពតមនអតថជនInfo, new RecordItemIndex(45));
            Validate.AttributeEqual(repo.Qt5QWindowToolSaveBits.Container.ពតមនអតថជនInfo, "Title", "ព័ត៌មានអតិថិជន");
            Delay.Milliseconds(100);
            
        }

#region Image Feature Data
        /// <summary>
        /// DO NOT REFERENCE THIS CODE  - auto generated
        /// </summary>
        CompressedImage ទតងបរបរសចរនតIN_Screenshot1
        { get { return repo.Qt5QWindowToolSaveBits.Container.ទតងបរបរសចរនតINInfo.GetScreenshot1(new Rectangle(0, 0, 148, 24)); } }

        /// <summary>
        /// DO NOT REFERENCE THIS CODE  - auto generated
        /// </summary>
        Imaging.FindOptions ទតងបរបរសចរនតIN_Screenshot1_Options
        { get { return Imaging.FindOptions.Parse("1;None;0,0,148,24;True;10000000;0ms"); } }

#endregion
    }
#pragma warning restore 0436
}
